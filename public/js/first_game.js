// First Enchantjs Game
// くまさんで色々
$(function(){
	/***************************
	 * グローバル定数定義
	 ***************************/
	// fps
	var game_fps = 60;

	// 1メートルを何pxとするか
	var m_px = 30;

	// 重力
	var g = 9.8;
	// 落下中の重力補完（遅く落下）
	var fall_g = 2.0;

	// スクリーンサイズ(px)
	var screen_width = 320;
	var screen_height = 400;

	// 指の現在位置
	var now_finger_x = 0;
	var now_finger_y = 0;

	// シーンのタッチイベントをロックするフラグ
	var scene_lock_flag = false;

	// 衝突判定のためのクマ格納配列
	var bears = [];

	/***************************
	 * enchantjs初期処理
	 ***************************/
	// 初期処理
	enchant();

	// メインループ、シーン用オブジェクト
	var game = new Core(screen_width, screen_height);

	// FPS
	game.fps = game_fps;

	// 素材の事前ロード
	game.preload("/img/kuma.png", "/img/graphic.png", "/img/icon0.png", "/img/effect0.gif", "/sound/shoot.mp3", "/sound/explosion.ogg");

	/***************************
	 * Node定義
	 ***************************/
	// 速度、加速度を持つ物体
	var Material = Class.create(Sprite, {
		/**
		 * @param {number} w              横幅
		 * @param {number} h              高さ
		 * @param {number} x              x軸の初期位置
		 * @param {number} y              y軸の初期位置
		 * @param {number} x_speed        x軸方向の初期速度
		 * @param {number} y_speed        y軸方向の初期速度
		 * @param {number} x_accel        x軸方向の初期加速度
		 * @param {number} y_accel        y軸方向の初期加速度
		 */
		initialize: function (w, h, x, y, x_speed, y_speed, x_accel, y_accel) {
			Sprite.call(this, w, h);
			/*====================
			 * パラメータ設定
			 *====================*/
			this.width = w;
			this.height = h;
			this.x = x;
			this.y = y;
			this.now_x = 0;
			this.now_y = 0;
			this.x_accel = x_accel;
			this.y_accel = y_accel;
			// 現在の速度
			this.now_x_speed = 0;
			this.now_y_speed = 0;

			// 加速度変更時のための基準位置、基準時間、基準速度
			this.base_x = x;
			this.base_y = y;
			this.base_age = 0;
			this.base_x_speed = x_speed;
			this.base_y_speed = y_speed;

			/*====================
			 * フレーム操作
			 *====================*/
			this.addEventListener('enterframe', function() {
				/*--------------------
				 * 現在の状態を格納
				 *--------------------*/
				var past_second = 0; 
				var move_x = 0; 
				var move_y = 0; 

				// 基準時間からの秒数
				past_second = (this.age - this.base_age) / game.fps;
				// 基準位置からのx, y変位
				move_x = (this.base_x_speed * past_second + 0.5 * this.x_accel * past_second * past_second) * m_px;
				move_y = (this.base_y_speed * past_second + 0.5 * this.y_accel * past_second * past_second) * m_px;
				// 設定予定のx, y軸現在位置
				this.now_x = this.base_x + move_x;
				this.now_y = this.base_y - move_y;
				// 現在のx, y方向の速度
				this.now_x_speed = this.base_x_speed + this.x_accel * past_second;
				this.now_y_speed = this.base_y_speed + this.y_accel * past_second;

				if (this.now_x >= screen_width || this.now_x <= 0 - this.width) {
					// 右端もしくは左端に行ったら削除
					this.remove();
				}

				// 計算した位置に変更
				this.x = this.now_x;
				this.y = this.now_y;
			});
		}
	});
	// クマ
	var Bear = Class.create(Material, {
		/**
		 * @param {number} kind           くまの種類（0:茶オス, 1:白オス, 2:茶メス）
		 * @param {number} x              x軸の初期位置
		 * @param {number} y              y軸の初期位置
		 * @param {number} x_speed        x軸方向の初期速度
		 * @param {number} y_speed        y軸方向の初期速度
		 * @param {number} x_accel        x軸方向の初期加速度
		 * @param {number} y_accel        y軸方向の初期加速度
		 * @param {number} bunbun_speed   腕振り速度(回/秒)
		 * @param {number} bye_speed      一番下に行った時に退場する速度
		 * @param {number} key            格納配列のキー
		 */
		initialize: function (kind, x, y, bunbun_speed, bye_speed, key) {
			/*====================
			 * パラメータ
			 *====================*/
			// サイズ(px)
			this.width = 32;
			this.height = 32;
			// 腕振り頻度fps
			this.bunbun_fps = Math.floor(game_fps / bunbun_speed);
			// 下端に達した時の捌ける方向とスピード
			this.bye_speed = bye_speed;
			// 下端到達フラグ
			this.reach_bottom_flag = false;
			// タッチされた数
			this.touch_count = 0;
			// タッチ無効
			this.notouch = false;
			// もがき中フラグ
			this.struggling = false;
			// 落下中フラグ
			this.falling = false;
			// もがくときの腕振り
			this.struggling_bunbun_speed = 30;
			// もがくときの腕振り頻度fps
			this.struggling_bunbun_fps = Math.floor(game_fps / this.struggling_bunbun_speed);
			// もがくときの移動速度
			this.struggling_x_speed = 1;
			this.struggling_y_speed = 2;
			// もがく時間
			this.struggling_time = 1;
			// もがき始めた時のタイムスタンプ(フレーム数)
			this.struggling_start_age = 0;

			/*====================
			 * 親クラス初期化
			 *====================*/
			Material.call(this, this.width, this.height, x, y, 0, 10, 0, -g);

			/*====================
			 * 画像設定
			 *====================*/
			this.image = game.assets["/img/kuma.png"];
			this.kind = kind;
			this.key = key;
			this.frame = this.kind * 5;

			// デバッグ表示
/*
			var label = new Label();
			label.x = Math.floor(x);
			label.y = Math.floor(y) - 32;
			game.rootScene.addChild(label);
*/


			/*====================
			 * フレーム操作
			 *====================*/
			this.addEventListener('enterframe', function() {
				/*--------------------
				 * 動作定義
				 *--------------------*/
				if (this.now_y >= screen_height - this.height && this.now_y_speed <= 0) {
					// 着陸した場合
					if (this.reach_bottom_flag == false) {
						this.byebye();
					}
					// 腕振り動作
					if (this.kind == 0 && this.bye_speed >= 3) {
						// 茶オスで速い場合はスケボー
						this.frame = 4;
					} else {
						this.byebyeBunbun();
					}
				} else {
					// 着陸前
					if (this.now_y_speed < 0 && this.struggling == false) {
						if (this.falling == false) {
							// 落下開始時に加速度変更
							this.fall();
						}
					}
				}
				if (this.struggling == true) {
					// タッチによるもがき中
					// 腕振り
					this.struggleBunbun();
					if (this.age - this.struggling_start_age > this.struggling_time * game_fps) {
						// 所定時間がたったらもがき解除
						this.struggleEnd();
						// 再び落下する(加速度変更)
						this.fall();
					}
				}
				// デバッグ表示
/*
				label.x = this.x;
				label.y = this.y - 32;
				label.text = Math.floor(this.x) + ":" + Math.floor(this.y);
*/
			});

			/*====================
			 * タッチイベント
			 *====================*/
			this.addEventListener('touchstart', function(e) {
				if (this.notouch == true) {
					return true;
				}
				this.touch_count++;
				if (this.touch_count >= 2) {
					// 戦闘機に変化
					this.becomeFighter();
				}
				// 指が触れたらもがく
				this.struggleStart();
				// シーンタッチは反応させない
				scene_lock_flag = true;
			});
		},
		fall : function () {
			// 泣き顔
			this.frame = this.kind * 5 + 3;
			// 落下は気持ち遅めの加速度
			this.base_age = this.age;
			this.base_x = this.x;
			this.base_y = this.y;
			this.base_x_speed = 0;
			this.base_y_speed = 0;
			this.x_accel = 0;
			this.y_accel = -fall_g;
			// フラグ変更
			this.falling = true;
		},
		moveTo : function (x, y) {
			this.x = x - this.width/2;
			this.y = y - this.height/2;
		},
		becomeFighter : function () {
			this.remove();
			var fighter = new Fighter(this.x, this.y);
			game.rootScene.addChild(fighter);
		},
		struggleStart : function () {
			this.base_age = this.age;
			this.base_x = this.x;
			this.base_y = this.y;
			this.base_x_speed = this.struggling_x_speed;
			this.base_y_speed = this.struggling_y_speed;
			this.x_accel = 0;
			this.y_accel = 0;
			// 普通顔
			this.frame = this.kind * 5;
			// もがき時間測定開始
			this.struggling_start_age = this.age;
			// フラグ変更
			this.struggling = true;
			this.notouch = true;
			this.falling = false;
		},
		struggleEnd : function () {
			this.struggling = false;
			this.notouch = false;
		},
		struggleBunbun : function () {
			this.frame = Math.floor(this.age / this.struggling_bunbun_fps) % 2 + this.kind * 5 + 1;
		},
		byebye : function () {
			this.base_age = this.age;
			this.base_x = this.x;
			this.base_y = screen_height - this.height;
			this.base_x_speed = this.bye_speed;
			this.base_y_speed = 0;
			this.x_accel = 0;
			this.y_accel = 0;
			// フラグ変更
			this.reach_bottom_flag = true;
			this.struggling = false;
			this.notouch = true;
		},
		byebyeBunbun : function () {
			this.frame = Math.floor(this.age / this.bunbun_fps) % 2 + this.kind * 5 + 1;
		},
		remove : function () {
			game.rootScene.removeChild(this);
			delete bears[this.key];
			delete this;
		}
	});

	// 戦闘機
	var Fighter = Class.create(Sprite, {
		initialize: function (x, y) {
			/*====================
			 * パラメータ
			 *====================*/
			// サイズ(px)
			this.width = 32;
			this.height = 32;
			// 弾発射中フラグ
			this.shooting_flag = false;

			Sprite.call(this, this.width, this.height);
			this.image = game.assets["/img/kuma.png"];
			this.frame = 15;
			this.x = x;
			this.y = y;

			this.addEventListener('touchstart', function (e) {
				this.x = e.x - this.width / 2;
				this.y = e.y - this.height / 2;
				this.shooting_flag = true;
				// シーンタッチは反応させない
				scene_lock_flag = true;
			});
			this.addEventListener('touchmove', function (e) {
				this.x = e.x - this.width / 2;
				this.y = e.y - this.height / 2;
			});
			this.addEventListener('touchend', function (e) {
				this.x = e.x - this.width / 2;
				this.y = e.y - this.height / 2;
				this.shooting_flag = false;
			});
			this.addEventListener('enterframe', function (e) {
				if (this.shooting_flag == true && this.age % 8 == 0) {
					var bullet = new Bullet(this.x + this.width / 2, this.y + this.height / 2);
					game.rootScene.addChild(bullet);
				}
			});
		}
	});

	// 弾丸
	var Bullet = Class.create(Material, {
		initialize: function (x, y) {
			/*====================
			 * パラメータ
			 *====================*/
			// サイズ(px)
			this.width = 16;
			this.height = 16;

			Material.call(this, this.width, this.height, x, y, 10, 0, 0, 0);
			this.image = game.assets["/img/graphic.png"];
			this.frame = 2;

			// 音
			var sound = game.assets["/sound/shoot.mp3"].clone();
			sound.play();

			this.addEventListener('enterframe', function (e) {
				for (var i in bears) {
					if (bears[i].intersect(this)) {
						// エフェクト
						var explosion = new Explosion(bears[i].x + bears[i].width / 2 - this.width / 2, bears[i].y + bears[i].height / 2 - this.height / 2);
						game.rootScene.addChild(explosion);
						this.remove();
						bears[i].remove();
						break;
					}
				}
			});
		},
		remove: function () {
			game.rootScene.removeChild(this);
			delete this;
		}
	});

	// 爆発
	var Explosion = Class.create(Sprite, {
		initialize: function (x, y) {
			/*====================
			 * パラメータ
			 *====================*/
			// サイズ(px)
			this.width = 16;
			this.height = 16;

			Sprite.call(this, this.width, this.height);
			this.image = game.assets["/img/effect0.gif"];
			this.frame = 0;
			this.x = x;
			this.y = y;

			this.frame_sequence = [0,1,2,3,4];
			this.frame_num = 0;

			// 音
			var sound = game.assets["/sound/explosion.ogg"].clone();
			sound.play();

			this.addEventListener('enterframe', function (e) {
				if (this.age % 2 == 0) {
					if (this.frame_num == this.frame_sequence.length) {
						this.remove();
					}
					this.frame = this.frame_sequence[this.frame_num];
					this.frame_num++;
				}
			});
		},
		remove: function () {
			game.rootScene.removeChild(this);
			delete this;
		}
	});

	/***************************
	 * Game定義
	 ***************************/
	game.onload = function(){
		// タップ中フラグ
		var tapping = false;
		// タップ開始時のフレーム数
		var tap_start_age = null;
		// ロングタップまでの秒数
		var until_long_tap_second = 0.5;

		// 画面にタッチ時
		game.rootScene.addEventListener('touchstart', function (e) {
			// 指の現在位置を記録
			now_finger_x = e.x;
			now_finger_y = e.y;
			
			if (scene_lock_flag == false) {
				// タッチロックされていなければクマ出現
				bears[game.frame] = addBear(e.x, e.y, game.frame);
				// ロングタップ判定のための値をとっておく。
				tapping = true;
				tap_start_age = this.age;
			} else {
				scene_lock_flag = false;
			}
		});
		game.rootScene.addEventListener('touchmove', function (e) {
			// 指の現在位置を記録
			now_finger_x = e.x;
			now_finger_y = e.y;
		});
		game.rootScene.addEventListener('touchend', function (e) {
			tapping = false;
		});
		game.rootScene.addEventListener('enterframe', function () {
			if (tapping == true && this.age - tap_start_age > until_long_tap_second * game_fps) {
				// タップしてから離さずに所定時間立っていたらクマを連続作成
				if (this.age % 3 == 0) {
					bears[game.frame] = addBear(now_finger_x, now_finger_y, game.frame);
				}
			}
		});
	};

	/**
	 * クマを追加
	 *
	 * @param {number} x   クマを出現させるx位置
	 * @param {number} y   クマを出現させるy位置
	 * @param {number} key 格納配列のキー
	 */
	var addBear = function (x, y, key) {
		// くまの種類をランダム定義
		var kind = Math.floor(Math.random() * 3);
		// 捌ける際のスピードをランダム定義
		var bye_speed = 1 + Math.floor(Math.random() * 6);
		// 捌ける際の腕振り速度はスピードに比例
		var bunbun_speed = bye_speed * 3;
		// 捌ける方向をランダム定義
		if (Math.floor(Math.random() * 2) == 1) {
			// 左方向
			bye_speed = bye_speed * -1;
		} else {
			// 右方向
			bye_speed = bye_speed * 1;
		}
		// クマ作成、追加
		var bear = new Bear(kind, x, y, bunbun_speed, bye_speed, key);
		game.rootScene.addChild(bear);

		return bear;
	};

	/**
	 * Core#start
	 * ゲームを開始する。この関数を実行するまで、ゲームは待機状態となる。
	 * 代わりに Core#debug を使うことで、デバッグモードで起動することができる。
	 * Core#pause(); で一時停止し、 Core#resume(); で再開することができる。
	 */
	game.start();
});
