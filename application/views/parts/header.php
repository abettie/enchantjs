<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php foreach ($js as $js_path) { ?> 
		<script src="<?= $js_path ?>"></script>
	<?php } ?>
	<title><?= $page_title ?></title>
</head>
<body>
<div class="container">
